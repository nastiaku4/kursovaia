﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; }

    [SerializeField] Button[] _buttons;
    [SerializeField] GameField _gameField;
    [SerializeField] UIController _uIController;
    [SerializeField] ParticleSystem[] _confetties;

    public bool IsGameOver { get; private set; }
    public int LevelIndex { get { return PlayerPrefs.GetInt("LevelIndex", 1);} set { PlayerPrefs.SetInt("LevelIndex", value); } }

    private void Awake()
    {
        Instance = this;
    }

    public void CheckAllButtons()
    {
        for (int i = 0; i < _buttons.Length; i++)
            if (!_buttons[i].IsActive)
                return;

        _gameField.OpenDoor();
    }

    public void GameOver()
    {
        IsGameOver = true;
        foreach (ParticleSystem particleSystem in _confetties)
            particleSystem.Play();
        _uIController.Win();
        LevelIndex++;
        if (LevelIndex >= 6)
            LevelIndex = 1;
    }

    public void NextLevel()
    {
        SceneManager.LoadScene(LevelIndex.ToString());
    }
}
