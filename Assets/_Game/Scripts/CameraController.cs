﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] Transform _playerTransform;
    [SerializeField] float _lerpSpeed;
    private Vector3 offset;

    void Start()
    {
        offset = transform.position - _playerTransform.position;
    }

    void FixedUpdate()
    {
        //transform.position = _playerTransform.position + offset;

        //transform.position = Vector3.Lerp(_playerTransform.position, _playerTransform.position + offset, _lerpSpeed);

        transform.position = Vector3.Lerp(transform.position, _playerTransform.position + offset, _lerpSpeed);
    }
}
