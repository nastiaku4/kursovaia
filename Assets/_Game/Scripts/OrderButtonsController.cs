﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderButtonsController : MonoBehaviour
{
    [SerializeField] Button[] _buttons;

    public void ResetButtons()
    {
        for(int i = 0; i < _buttons.Length; i++)
        {
            _buttons[i].UnPressButton();
        }
    }
}
