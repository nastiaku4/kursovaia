﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderButton : Button
{
    [SerializeField] int _orderIndex;
    [SerializeField] OrderButtonsController _buttonsController;

    static int _currentIndex;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            Press();
    }

    void Press()
    {
        if (_orderIndex == _currentIndex)
        {
            PressButton();
            _currentIndex++;    
        }
        else if(!IsActive)
        {
            _currentIndex = 0;
            _buttonsController.ResetButtons();
        }
    }
}
