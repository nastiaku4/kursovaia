﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameField : MonoBehaviour
{
    [SerializeField] GameObject _door;
    [SerializeField] GameObject _path;

    public void OpenDoor()
    {
        _door.transform.DOLocalMoveY(0.2f, 1f).SetEase(Ease.Linear);
        _path.transform.DOScaleZ(2.8f, 2f).SetEase(Ease.Linear);
    }

}
