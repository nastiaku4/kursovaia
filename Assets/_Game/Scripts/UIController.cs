﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] GameObject _winPanel;
    [SerializeField] GameObject _joystick;
    [SerializeField] Text _levelText;

    public void Win()
    {
        _joystick.SetActive(false);
        _winPanel.SetActive(true);
        _levelText.text = "Leve " + GameController.Instance.LevelIndex.ToString();
    }
}
