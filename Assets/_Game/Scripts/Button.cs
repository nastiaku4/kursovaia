﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Button : MonoBehaviour
{
    protected MeshRenderer _meshRenderer;

    float _activeButtonY = -0.5f;
    float _notActiveButtonY = -0.47f;

    public bool IsActive { get; protected set; }

    private void Start()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
    }

    public void PressButton()
    {
        transform.DOMoveY(_activeButtonY, 0.3f).SetEase(Ease.Linear).OnComplete(()=> 
        {
            IsActive = true;
            _meshRenderer.material.color = Color.green;
            GameController.Instance.CheckAllButtons();
        });
    }

    public void UnPressButton()
    {
        transform.DOMoveY(_notActiveButtonY, 0.3f).SetEase(Ease.Linear).OnComplete(() => 
        {
            IsActive = false;
            _meshRenderer.material.color = Color.red;
        });

    }
}
