﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleButton : Button
{
    private void OnTriggerStay(Collider other)
    {
        if (IsActive)
            return;
        GameObject go = other.gameObject;
        if (go.tag == "Player" || go.tag == "Box")
            PressButton();
    }

    private void OnTriggerExit(Collider other)
    {
        GameObject go = other.gameObject;
        if (go.tag == "Player" || go.tag == "Box")
            UnPressButton();
    }
}
