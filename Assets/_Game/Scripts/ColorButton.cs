﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorButton : Button
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            if (other.GetComponent<PlayerController>().CheckColor() == _meshRenderer.material.color)
                PressButton();
        }
    }
}
