﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] VariableJoystick _variableJoystick;
    [SerializeField] float _speed;
    [SerializeField] Transform _pointForBox;
    [SerializeField] SkinnedMeshRenderer _skinnedMeshRenderer;

    Animator _animator;
    Rigidbody _rb;
    Vector3 _direction = Vector3.zero;
    GameObject _box;

    bool _isStop = true;
    bool _isWithBox = false;
    bool _canPickUpBox = true;

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (GameController.Instance.IsGameOver)
            return;
        if (_variableJoystick.Vertical != 0 && _variableJoystick.Horizontal != 0)
        {
            if (_isStop)
            {
                if(_isWithBox)
                    _animator.SetTrigger("RunBox");
                else
                    _animator.SetTrigger("Run");
                _rb.isKinematic = false;
                _isStop = false;
            }
            _direction = Vector3.forward * _variableJoystick.Vertical + Vector3.right * _variableJoystick.Horizontal;
            _rb.velocity = _direction * _speed;
            transform.rotation = Quaternion.LookRotation(_direction, Vector3.up);
        }
        else if (!_isStop)
        {
            transform.rotation = Quaternion.LookRotation(_direction, Vector3.up);
            _rb.isKinematic = true;
            if (_isWithBox)
                _animator.SetTrigger("IdleBox");
            else
                _animator.SetTrigger("Idle");
            _isStop = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Finish")
        {
            _animator.SetInteger("DanceIndex", Random.Range(0, 2));
            _animator.SetTrigger("Dance");
            GameController.Instance.GameOver();
        }
        else if (other.tag == "Button" && _isWithBox)
        {
            ThrowBox();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject go = collision.gameObject;
        if (go.tag == "Box" && _canPickUpBox)
        {
            _box = go;
            PickUpBox();
        }
        else if (go.tag == "ColorPlate")
            _skinnedMeshRenderer.material.color = go.GetComponent<MeshRenderer>().material.color;

    }


    void PickUpBox()
    {
        _box.GetComponent<Collider>().isTrigger = true;
        _box.GetComponent<Rigidbody>().isKinematic = true;
        _isWithBox = true;
        _box.transform.SetParent(transform);
        _box.transform.position = _pointForBox.transform.position;
        _animator.SetTrigger("RunBox");
        _canPickUpBox = false;
    }

    void ThrowBox()
    {
        _box.transform.SetParent(null);
        _box.GetComponent<Collider>().isTrigger = false;
        _box.GetComponent<Rigidbody>().isKinematic = false;
        _animator.SetTrigger("Run");
        _isWithBox = false;
    }

    public Color CheckColor()
    {
        return _skinnedMeshRenderer.material.color;
    }
}
